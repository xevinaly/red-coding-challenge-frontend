import React from 'react';
import axios from 'axios';
import Page from "../../components/Page";
import SearchBar from "material-ui-search-bar";
import Button from '@mui/material/Button';
import Stack from '@mui/material/Stack';
import TextField from '@mui/material/TextField';
import Select, { SelectChangeEvent } from '@mui/material/Select';
import InputLabel from '@mui/material/InputLabel';
import MenuItem from '@mui/material/MenuItem';
import FormControl from '@mui/material/FormControl';
import Box from '@mui/material/Box';
import AddIcon from '@mui/icons-material/Add';
import { DataGrid, GridColDef, GridSelectionModel } from '@mui/x-data-grid';
import DeleteForeverIcon from '@mui/icons-material/DeleteForever';
import Typography from '@mui/material/Typography';
import Modal from '@mui/material/Modal';

type Order = {
    orderId: number,
    orderType: string,
    customerName: string, 
    createdDate: string, 
    createdByUserName: string
}

type HomeProps = {}

type HomeState = {

    orders: Order[],
    filter: string,
    customer: string,
    type: string,
    pageSize: number,
    delete: GridSelectionModel,
    open: boolean
}

const columns: GridColDef[] = [
    { field: 'orderId', headerName: 'Order ID', width: 100 },
    { field: 'createdDate', headerName: 'Creation Date', width: 230 },
    { field: 'createdByUserName', headerName: 'Created By', width: 160 },
    { field: 'orderType', headerName: 'Order Type', width: 160 },
    { field: 'customerName', headerName: 'Customer', width: 160 }
];

const style = {
    position: 'absolute' as 'absolute',
    top: '50%',
    left: '50%',
    transform: 'translate(-50%, -50%)',
    width: 400,
    bgcolor: 'background.paper',
    border: '2px solid #000',
    boxShadow: 24,
    p: 4,
};

const orderTypes = [
    {
      value: '0',
      label: 'Standard',
    },
    {
      value: '1',
      label: 'SaleOrder',
    },
    {
      value: '2',
      label: 'PurchaseOrder',
    },
    {
      value: '3',
      label: 'TransferOrder',
    },
    {
      value: '4',
      label: 'ReturnOrder',
    },
  ];

class Home extends React.Component<HomeProps, HomeState> {
    constructor(props: HomeProps | Readonly<HomeProps>) {
        super(props);

        this.state = {
            orders: [],
            filter: '',
            customer: '',
            type: '',
            pageSize: 25,
            delete: [],
            open: false
        };
    }

    componentDidMount() {
        axios.get(`https://localhost:5001/order`)
            .then(res => {
                const orders = res.data;
                this.setState({ orders });
            })
    }

    render() {
        const orders = this.state.orders.map(order => {
            return {
                id: order.orderId,
                orderId: order.orderId,
                orderType: order.orderType,
                customerName: order.customerName, 
                createdDate: order.createdDate, 
                createdByUserName: order.createdByUserName
            };
        }).filter(order => `${order.orderId}`.match(new RegExp(this.state.filter)));

        return (
            <Page headerTitle={"Home"}>  
                <Stack
                direction={{ sm: 'column', md: 'row' }}
                justifyContent="flex-start"
                alignItems="center"
                spacing={{ xs: 1, sm: 2, md: 4 }}
                >
                    <SearchBar
                        value={this.state.filter}
                        onChange={(newValue) => this.setState({filter: newValue })}
                        onCancelSearch={() => this.setState({filter: '' })}
                        className="searchBar"
                        placeholder="Search By ID"
                    />
                    {this.state.delete.length ? (
                        <Button
                            className="100-button"
                            variant="contained"
                            color="error"
                            onClick={() => {
                                this.state.delete.forEach(order => axios.delete(`https://localhost:5001/order/${order}`));
                            
                                this.setState({
                                    delete: [],
                                    orders: orders.filter(order => !this.state.delete.includes(order.orderId))
                                });
                            }}
                        >
                            <DeleteForeverIcon/>

                            Delete Order
                        </Button>
                    ) : (
                        <Button className="100-button" variant="contained"  onClick={() => this.setState({ open: true })}><AddIcon /> Create Order</Button>
                    )}

                    <Stack direction="row" spacing={4}>
                        <Box sx={{ minWidth: 120 }}>
                            <FormControl fullWidth size="small">
                                <InputLabel id="customer-label">Customer</InputLabel>
                                <Select
                                    labelId="customer-label"
                                    id="customer"
                                    value={this.state.customer}
                                    label="Customer"
                                    onChange={(event: SelectChangeEvent) => {
                                        const customer = event.target.value as string;
                                        const type = this.state.type;

                                        let url;

                                        if (customer) url = `https://localhost:5001/order?customerName=${customer}${type ? `&orderType=${type}` : ''}`;
                                        else if (type) url = `https://localhost:5001/order?orderType=${type}`;
                                        else url = 'https://localhost:5001/order';

                                        axios.get(url)
                                            .then(res => {
                                                const orders = res.data;
                                                this.setState({ customer, orders });
                                            });
                                    }}
                                >
                                    <MenuItem value="">
                                        <em>None</em>
                                    </MenuItem>
                                    {
                                        [...new Set<string>(orders.map((order: Order) => order.customerName))].map(customer =>
                                            <MenuItem value={customer}>{customer}</MenuItem>
                                        )
                                    }
                                </Select>
                            </FormControl>
                        </Box>
                        <Box sx={{ minWidth: 120 }}>
                            <FormControl fullWidth size="small">
                                <InputLabel id="type-label">Type</InputLabel>
                                <Select
                                    labelId="type-label"
                                    id="type"
                                    value={this.state.type}
                                    label="Type"
                                    onChange={(event: SelectChangeEvent) => {
                                        const type = event.target.value as string;
                                        const customer = this.state.customer;

                                        let url;

                                        if (customer) url = `https://localhost:5001/order?orderType=${type}${customer ? `&customerName=${customer}` : ''}`;
                                        else if (customer) url = `https://localhost:5001/order?customerName=${customer}`;
                                        else url = 'https://localhost:5001/order';

                                        axios.get(url)
                                            .then(res => {
                                                const orders = res.data;
                                                this.setState({ type, orders });
                                            });
                                    }}
                                >
                                    <MenuItem value="">
                                        <em>None</em>
                                    </MenuItem>
                                    {
                                        [...new Set<string>(orders.map((order: Order) => order.orderType))].map(type =>
                                            <MenuItem value={type}>{type}</MenuItem>
                                        )
                                    }
                                </Select>
                            </FormControl>
                        </Box>
                    </Stack>
                </Stack>

                <br />

                <div style={{ height: 400, width: '100%' }}>
                    <DataGrid
                        rows={orders}
                        columns={columns}
                        pageSize={this.state.pageSize}
                        onPageSizeChange={newPageSize => this.setState({pageSize: newPageSize })}
                        rowsPerPageOptions={[5, 10, 25, 100]}
                        checkboxSelection
                        onSelectionModelChange={selected => this.setState({delete: selected })}
                    />
                </div>

                <Modal
                    open={this.state.open}
                    onClose={() => this.setState({ open: false })}
                    aria-labelledby="modal-modal-title"
                    aria-describedby="modal-modal-description"
                >
                    <Box sx={style}>
                        <Typography id="modal-modal-title" variant="h6" component="h2">
                            Create Order
                        </Typography>
                        <Typography id="modal-modal-description" sx={{ mt: 2 }}>
                            To create a new order, submit a customer, creator, and order type.
                        </Typography>

                        <form onSubmit={event => {
                            event.preventDefault();

                            if (event.target instanceof HTMLFormElement) {
                                const data = new FormData(event.target);

                                console.log(data.get('customer'));

                                axios.post(`https://localhost:5001/order`, {
                                    customer: data.get('customer'),
                                    creator: data.get('creator'),
                                    type: parseInt(data.get('type') as string)
                                }).then(res => {
                                    const order = res.data;
                                    this.setState({
                                        open: false,
                                        orders: [...orders, order]
                                    });
                                });
                            }
                        }}>
                            <Stack direction="column" spacing={4}>
                                <TextField
                                    label="Customer Name"
                                    name="customer"
                                    variant="standard"
                                    required
                                />
                                <TextField
                                    label="Creator Name"
                                    name="creator"
                                    variant="standard"
                                    required
                                />
                                <TextField
                                    select
                                    label="Order Type"
                                    name="type"
                                    defaultValue="0"
                                    required
                                >
                                    {orderTypes.map(type => (
                                        <MenuItem key={type.value} value={type.value}>
                                        {type.label}
                                        </MenuItem>
                                    ))}
                                </TextField>
                                <Button className="100-button" variant="contained"  type="submit">
                                    Submit
                                </Button>
                            </Stack>
                        </form>
                    </Box>
                </Modal>
            </Page>
        );
    }
}

export default Home; 